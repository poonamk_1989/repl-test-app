import React from 'react';

import { Session } from '@replit/clui-session';
import { forEach } from '@replit/clui-gql';

import MutationPrompt from '../commands/run';
import clear from '../commands/clear';
import Prompt from './Prompt';
import data from '../constant.json';

const applyRunFn = ({ command }) => {
  console.log('mutation', command.mutation);
  if (!command.commands) {
    command.run = () => <MutationPrompt />;
  }
};

const Terminal = () => {
  let command;

  if (data && data.commands) {
    command = data.commands;
    forEach(command, applyRunFn);
    command.commands = {
      ...command.commands,
      clear,
    };
  }

  return (
    <div className="terminal">
      <Session>
        <Prompt command={command} />
      </Session>
    </div>
  );
};

export default Terminal;
