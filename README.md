CLUI is a collection of JavaScript libraries for building command-line interfaces with context-aware autocomplete.
As mentioned in the problem statement constant.json is the file whose data is being read

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## Packages

### `@replit/clui-input`

### `@replit/clui-session`

### `@replit/clui-gql`
